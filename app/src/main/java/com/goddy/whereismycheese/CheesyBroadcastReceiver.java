package com.goddy.whereismycheese;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class CheesyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        CheesyService.enqueueWork(context, intent);
    }
}
