package com.goddy.whereismycheese;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheesyFindDialog extends Dialog implements View.OnClickListener {

    public Activity context;
    public Button pickupButton, exitButton;
    public TextView cheeseNote;
    public INoteDialogListener listener;

    public CheesyFindDialog(Activity context, INoteDialogListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_find_note);

        exitButton = (Button) findViewById(R.id.exitDialogButton);
        cheeseNote = (TextView) findViewById(R.id.noteText);
        pickupButton = (Button) findViewById(R.id.pickupCheeseButton);

        exitButton.setOnClickListener(this);
        pickupButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.pickupCheeseButton:
                listener.onPickUpCheese();
                dismiss();
                break;
            case R.id.exitDialogButton:
                dismiss();
                break;
            default:
                break;
        }
    }

    public interface INoteDialogListener
    {
        public void onPickUpCheese();
    }
}

