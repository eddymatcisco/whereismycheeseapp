package com.goddy.whereismycheese;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

class DataBaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "treasure_db";


    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(CheesyTreasure.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + CheesyTreasure.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public long insertCheesyTreasure(Double latitude, Double longitude, String note) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CheesyTreasure.COLUMN_LATITUDE, latitude);
        values.put(CheesyTreasure.COLUMN_LONGITUDE, longitude);
        values.put(CheesyTreasure.COLUMN_NOTE, note);

        // insert row
        long id = db.insert(CheesyTreasure.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public CheesyTreasure getTreasure(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(CheesyTreasure.TABLE_NAME,
                new String[]{CheesyTreasure.COLUMN_ID, CheesyTreasure.COLUMN_LATITUDE, CheesyTreasure.COLUMN_LONGITUDE, CheesyTreasure.COLUMN_NOTE},
                CheesyTreasure.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object

        CheesyTreasure cheesyTreasure = new CheesyTreasure(
                cursor.getInt(cursor.getColumnIndex(CheesyTreasure.COLUMN_ID)),
                cursor.getDouble(cursor.getColumnIndex(CheesyTreasure.COLUMN_LATITUDE)),
                cursor.getDouble(cursor.getColumnIndex(CheesyTreasure.COLUMN_LONGITUDE)),
                cursor.getString(cursor.getColumnIndex(CheesyTreasure.COLUMN_NOTE))
        );

        // close the db connection
        cursor.close();

        return cheesyTreasure;
    }

    public List<CheesyTreasure> getAllTreasures() {
        List<CheesyTreasure> treasures = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + CheesyTreasure.TABLE_NAME + " ORDER BY " +
                CheesyTreasure.COLUMN_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CheesyTreasure cheesyTreasure = new CheesyTreasure();
                cheesyTreasure.setId(cursor.getInt(cursor.getColumnIndex(CheesyTreasure.COLUMN_ID)));
                cheesyTreasure.setLatitude(cursor.getDouble(cursor.getColumnIndex(CheesyTreasure.COLUMN_LATITUDE)));
                cheesyTreasure.setLongitude(cursor.getDouble(cursor.getColumnIndex(CheesyTreasure.COLUMN_LONGITUDE)));
                cheesyTreasure.setNote(cursor.getString(cursor.getColumnIndex(CheesyTreasure.COLUMN_NOTE)));
                treasures.add(cheesyTreasure);

            } while (cursor.moveToNext());
        }
        cursor.close();
        // close db connection
        db.close();

        // return notes list
        return treasures;
    }

    public void deleteCheesyTreasure(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(CheesyTreasure.TABLE_NAME, CheesyTreasure.COLUMN_ID + " = ?",
                new String[]{id});
        db.close();
    }
}