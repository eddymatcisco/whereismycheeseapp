package com.goddy.whereismycheese;

/**
 * Cheezy Treasures are such a delight
 */

public class CheesyTreasure {

    public static final String TABLE_NAME = "treasure";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_NOTE = "note";

    private long id;
    private double latitude;
    private double longitude;
    private String note;

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_LATITUDE + " DOUBLE,"
                    + COLUMN_LONGITUDE + " DOUBLE,"
                    + COLUMN_NOTE + " TEXT "
                    + ")";

    public CheesyTreasure(){}

    public CheesyTreasure(long id, double latitude, double longitude, String note) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.note = note;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    // For simplicities sake, we will just assume all cheesy treasures have unique notes
    public boolean equals(CheesyTreasure other){
        return this.note.equals(other.note);
    }
}
